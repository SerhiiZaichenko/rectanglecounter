﻿using RectCount.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RectCount
{
    public static class PointsGenerator
    {
        /// <summary>
        /// Generates randomly shuffled list of points
        /// max-min must be bigger then count
        /// </summary>
        /// <param name="count">Count of points</param>
        /// <returns>Randomly shuffled list of points</returns>
        public static List<Point> GeneratePoints(int count, int min, int max)
        {
            var numbers = GenerateNumbers(count, min, max);
            return ConvertNumbersToPoints(numbers);
        }

        /// <summary>
        /// Coverts numbers to points using inverted Cantor pairing function
        /// </summary>
        /// <param name="numbers">List of numbers</param>        
        /// <returns>List of points</returns>
        public static List<Point> ConvertNumbersToPoints(List<int> numbers)
        {
            if (numbers is null) throw new ArgumentNullException("numbers");
            List<Point> result = new List<Point>();
            foreach (var k in numbers)
            {
                double w = Math.Floor((Math.Sqrt(1 + 8 * k) - 1) / 2);
                double y = k - (w + 1) * w / 2;
                double x = w - y;
                result.Add(new Point(x, y));
            }
            return result;
        }

        /// <summary>
        /// Generate randomly shuffled list of unique numbers in range from min to max
        /// </summary>
        ///<exception cref="ArgumentOutOfRangeException">Throws if range or count is illegal</exception>
        /// <returns>Shuffled list of unique numbers</returns>
        static List<int> GenerateNumbers(int count, int min, int max)
        {
            Random random = new Random();
            if (max <= min || count < 0 ||
                    (count > max - min && max - min > 0))
            {
                throw new ArgumentOutOfRangeException("Range " + min + " to " + max +
                        " (" + ((Int64)max - (Int64)min) + " values), or count " + count + " is illegal");
            }
            HashSet<int> candidates = new HashSet<int>();
            for (int top = max - count; top < max; top++)
            {
                if (!candidates.Add(random.Next(min, top + 1)))
                {
                    candidates.Add(top);
                }
            }
            List<int> result = candidates.ToList();
            // shuffle results
            for (int i = result.Count - 1; i > 0; i--)
            {
                int k = random.Next(i + 1);
                int tmp = result[k];
                result[k] = result[i];
                result[i] = tmp;
            }
            return result;
        }
    }
}
