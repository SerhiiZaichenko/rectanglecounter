﻿using RectCount.Entities;
using System;
using System.Collections.Generic;

namespace RectCount
{
    public static class RectCounter
    {
        /// <summary>
        /// Calculates number of unique rectangles that can be formed by points
        /// </summary>
        /// <param name="points">List of points</param>
        /// <returns>Number of rectangles</returns>
        public static int CountRectangles(List<Point> points)
        {
            if (points is null) throw new ArgumentNullException("points");

            List<PointsPair> pairs = GeneratePointsPairs(points);
            int RectCount = 0;
            for (int i = 0; i < pairs.Count - 1; i++)
            {
                for (int j = i + 1; j < pairs.Count; j++)
                {
                    if (IsRectangle(pairs[i], pairs[j]))
                    {
                        RectCount++;
                        Console.WriteLine(RectCount + ". " +
                            pairs[i].First.X + " " + pairs[i].First.Y + "  " + pairs[j].First.X + " " + pairs[j].First.Y + "  " +
                            pairs[i].Second.X + " " + pairs[i].Second.Y + "  " + pairs[j].Second.X + " " + pairs[j].Second.Y + " - RECTANGLE");
                    }
                }
            }
            return RectCount;
        }
        /// <summary>
        /// Checks if pairs of points form rectangle
        /// </summary>
        static bool IsRectangle(PointsPair first, PointsPair second)
        {
            return first.Middle.X == second.Middle.X && first.Middle.Y == second.Middle.Y && first.Length == second.Length;
        }
       
        /// <summary>
        /// Generates all possible unique pairs of points
        /// </summary>
        /// <returns>List of points pairs</returns>
        static List<PointsPair> GeneratePointsPairs(List<Point> points)
        {
            List<PointsPair> pairs = new List<PointsPair>();
            for (int i = 0; i < points.Count - 1; i++)
            {
                for (int j = i + 1; j < points.Count; j++)
                {
                    pairs.Add(new PointsPair(points[i], points[j]));
                }
            }
            return pairs;

        }
    }
}
