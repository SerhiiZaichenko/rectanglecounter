﻿using System;

namespace RectCount.Entities
{
    public class PointsPair
    {
        public Point First { get; }
        public Point Second { get; }
        public Point Middle { get; }
        public double Length { get; }

        public PointsPair(Point first, Point second)
        {
            First = first;
            Second = second;
            Length = CalcLength();
            Middle = CalcMiddle();
        }

        private Point CalcMiddle()
        {
            return new Point((First.X + Second.X) / 2, (First.Y + Second.Y) / 2);
        }

        private double CalcLength()
        {
            return Math.Sqrt((First.X - Second.X) * (First.X - Second.X) + (First.Y - Second.Y) * (First.Y - Second.Y));
        }
    }
}
