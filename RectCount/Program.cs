﻿using System;
using System.Collections.Generic;

namespace RectCount
{
    static class Program
    {
        static void Main(string[] args)
        {
            Random random = new Random();
            var count = random.Next(0,100);
            var max = random.Next(count, 150);
            var Points = PointsGenerator.GeneratePoints(count, 0,max);
            foreach (var point in Points)
            {                
                Console.WriteLine(point.X + " " + point.Y);
            }
            Console.WriteLine("Rectangles count:"+RectCounter.CountRectangles(Points));
           
        }
    }
}
